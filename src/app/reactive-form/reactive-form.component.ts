import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormControl, FormControlName } from '@angular/forms';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
})

export class ReactiveFormComponent implements OnInit, OnDestroy {
  cutoffsList = [
    {
      id: 1,
      name: 'Water'
    },
    {
      id: 2,
      name: 'Juice'
    },
    {
      id: 3,
      name: 'Ice Tea'
    },
    {
      id: 4,
      name: 'Other'
    }
  ];
  

  cutoffsForm: FormGroup;
  formSubscription: any;

  ngOnInit() {
    this.cutoffsForm = new FormGroup({
      'cutoffs': new FormControl(null),
      'selectedCutoffs': new FormArray([])
    });

    this.formSubscription = (<FormArray>this.cutoffsForm.get('cutoffs')).valueChanges.subscribe(
      (value) => {
        console.log(value);

        this.onSelectChanged();
      }
    );
  }

  onSelectChanged() {
    const control = new FormControl(null);
    (<FormArray>this.cutoffsForm.get('selectedCutoffs')).push(control);
  }

  onSubmit() {
    console.log(this.cutoffsForm.controls);
  }

  ngOnDestroy() {
    this.formSubscription.unsubscribe();
  }
}
