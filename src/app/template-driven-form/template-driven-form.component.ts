import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ArrayType } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
})

export class TemplateDrivenFormComponent {
  @ViewChild('form') cutoffsForm: NgForm;
  cutoffsList: Array<Object> = [
    {
      id: 11,
      name: 'Water'
    },
    {
      id: 12,
      name: 'Juice'
    },
    {
      id: 13,
      name: 'Ice Tea'
    },
    {
      id: 14,
      name: 'Other'
    }
  ];

  cutoffFields: Array<Object>;

  selectChanged(param: Array<Object>) {
    const cutoffs = [];
    (<Array<Object>>param).forEach(element => {
      const cutoff = this.cutoffsList.find(item => (item['id'] == element));

      const newCutoffInput = {
        id: element,
        name: cutoff['name'],
        value: null
      };

      cutoffs.push(newCutoffInput);
    });

    if (this.cutoffFields) {
      // Check which input elements to remove
      (<Array<Object>>this.cutoffFields).forEach((element, index) => {
        const entry = cutoffs.find(item => (item['id'] === element['id']));
        if (!entry) {
            this.cutoffFields.splice(index, 1);
        }
      });

      // Check for new input elements
      (<Array<Object>>cutoffs).forEach(element => {
        const entry = this.cutoffFields.find(item => (item['id'] === element['id']));
        if (!entry) {
            this.cutoffFields.push(element);
        }
      });
    } else {
      this.cutoffFields = cutoffs;
    }
  }

  onSubmit() {
    console.log(this.cutoffFields);
  }
}
